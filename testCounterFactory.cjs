//importing counterFactory function
const counterFactoryFunc = require('./counterFactory.cjs');
const counterFactory = counterFactoryFunc();

//Consoling the output
console.log(counterFactory.increment());
console.log(counterFactory.increment());
console.log(counterFactory.increment());
console.log(counterFactory.decrement());
console.log(counterFactory.increment());
