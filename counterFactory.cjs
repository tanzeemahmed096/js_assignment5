//Creating a countFactory function
module.exports = function counterFactory() {
  //Initializing counter variable
  let count = 0;

  //Increment method which has a closure scope
  function increment() {
    count++;
    return count;
  }

  //Decrement method which will decrease counter variable
  function decrement() {
      count--;
      return count;
  }

  return { increment, decrement };
};
