//Creating limit call function
module.exports = function limitFunctionCallCount(callBack, n) {
  //Checking if the call back is of function type
  if (arguments.length === 0 || typeof arguments[0] !== "function" || !n) {
    throw new Error("No arguments provided or no call back provided");
  }

  let count = 0;
  //Function that will invoke call back;
  function returnValue(...args) {
    count++;
    if(count <= n){
      return callBack(...args);
    }else{
      return null;
    }
  }
  return returnValue;
};

