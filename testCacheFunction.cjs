//Importing the cache function
const cacheFunc = require('./cacheFunction.cjs');
const cacheCheck = cacheFunc(callBack);

function callBack(...args){
    console.log("Fn executed");
    return arguments[0] + arguments[1];
}

//Consoling the output
console.log(cacheCheck(1, 2));
console.log(cacheCheck(1, 2));
console.log(cacheCheck(2, 3));