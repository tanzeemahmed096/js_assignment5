//Creating cache function
module.exports = function cacheFunction(callBack) {
  //Checking if the call back is of function type
  if (arguments.length === 0 || typeof arguments[0] !== "function") {
    throw new Error("No arguments provided or no call back provided");
  }

  let cache = {};
  function invokeCB(...args) {
    const arg = JSON.stringify(args);
    if (arg in cache) {
      return cache[arg];
    } else {
      let res = callBack(...args);
      cache[arg] = res;
      return res;
    }
  }

  return invokeCB;
};
